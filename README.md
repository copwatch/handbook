# Copwatch Handbook

This handbook is a collection of researched knowledge/tools/tactics/techniques for copwatching, documenting and possibly preventing police brutality and surveillance.

## Why this, why now?

Simply because there wasn't a unified guide on the counter-police tactics that were tested for what work and what didn't, and to learn the complexity of police surveillance that's beyond the scope of documented conventional copwatch, and expanding on the available tools can ease the options for efficiently yet anonymously for both individuals and collectives to work with.

When they watching us, we can watch them back.

Police forces in the 21st centuries have combined not only counterinsurgency tactics but also the arsenals from military surveillance, such as drones mounted FLIR thermal cam with gait/facial recognition, or civvie plane with cell site simulator (CSS) such as DirtBox, Stingray or TriggerFish that can simultaneously monitor up to 10,000 devices. While we still haven't had workable countermeasure for CSS, it doesn't mean we cannot track them, and therefore by tracking their actions, this allows for preventing or even evading their tools/tactics/techniques.

This handbook doesn't stop at counter-surveillance but also would be regularly updated to expand on physical tactics that police utilised.

**`[Disclaimer]`** Copwatch Handbook is not meant to be a catch-all guide, but to encourage the learners to expand on these, modify and form their own guides that applicable to their local.

## Everyday Apps - Tracking police movements and surveillance

Documenting police with your phone/tablet is not just livestreaming them brutalising people. There is existing tools that can be utilised as crowdsourced cop trackers, the issues with some of these tool is still privacy but this can be mitigated with location services spoofing or restricted permission.

 - `Waze` (Android/iOS) is right off the bat the de-facto crowdsourced copwatch app that has been in existence for years. Users can tag the location of them in real time where the police have been at, and they can vote on the validity of it. [They also have a live map for non-users.](https://www.waze.com/live-map)

 - `Where Are the Eyes` (Android) can tag surveillance cams by standing near or right below it, save the location and it will be stored in local and [public database](https://eyes.daylightingsociety.org/).

 - `OpenDroneID OSM` (Android) is a Remote ID mapper. https://github.com/opendroneid/receiver-android

 - `Drone Scanner` ([Android](https://play.google.com/store/apps/details?id=cz.dronetag.dronescanner)/iOS) track [remote ID](https://hackaday.com/2023/03/26/loudmouth-dji-drones-tell-everyone-where-you-are/) broadcasted by drones that enabled it. Utilising the tech it's possible tracking the operator's location.

 - `Drone Watcher` (Android) [tracks for 2.4 GHz WiFi signals](https://apkpure.com/drone-watcher-app/com.dronewatcher.dronewatcher) beaming from nearby drones by identifying their comm ID protocols.

 - `Scanner Radio` [(Android/iOS)](https://scannerradio.app/index.html) does what it described that can scan for online feeds of police/emergency channels. *Advertisements on this app can be bypassed with [AdGuard](https://adguard.com/).*

 - `Snoop Snitch` (root/Android) is kinda workable CSS/IMSI-catcher dectector that can warn about GSM/LTE CSS. [A note on this is that no other detector app was able to detect modernised CSS with complex spoofing.](https://www.cs.ru.nl/bachelors-theses/2016/Bauke_Brenninkmeijer___4366298___Catching_IMSI-catcher-catchers.pdf)

 - `Qz` ([Android](https://fossdroid.com/a/qz.html)) an unique app that can scan for [TMC (Traffic Message Channel)](https://en.wikipedia.org/wiki/Traffic_message_channel) to detect road conditions, road blocks and constructions.

 - `RadioBeacon` [(Android)](https://fossdroid.com/a/radiobeacon.html) is useful for mapping cell towers and hotspots in real time.

---

TBC
